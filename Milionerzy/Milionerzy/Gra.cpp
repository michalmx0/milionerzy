#include "Gra.h"

Gra::Gra() //konstruktor
{
	
	//Konsola::setCoding(CP_UTF8);
	this->aktualnePytanie = 0;
	this->liczbaPytan = this->progi.wczytajZPliku();
}

Gra::~Gra()
{
}

void Gra::rozpocznij()
{
	this->aktualnePytanie = 0;
	this->wczytajPytania();
	string nick;
	int wiek;
	cout << "Podaj nick:  ";
	cin >> nick;
	cout << "Podaj swoj wiek:  ";
	cin >> wiek;
	while (wiek <= 0)
	{
		cin.clear();
		cin.ignore(INT_MAX, '\n');
		cout << "Bledny wiek!!! Podaj jescze raz: ";
		//cout << "Wiek mniejszy lub rowny 0\nPodaj swoj wiek:  ";
		/*while (getchar() != '\n')
			;*/
		cin >> wiek;
	}

	this->gracz = new Gracz(nick, wiek);

	while (this->aktualnePytanie != this->liczbaPytan - 1)
	{
		//this->gracz->wyswietl_podpowiedzi();
		this->gracz->pytanie(*this->generujPytanie());
		this->sprawdzOdpowiedz(this->gracz->odpowiedz());
	}
}

Pytanie *Gra::generujPytanie()
{
	this->aktualnePytanie++;
	return &this->pytania.at(this->aktualnePytanie);
}

void Gra::sprawdzOdpowiedz(char odp)
{
	switch (odp)
	{
	case 'T':
		this->gracz->uzyjKola(this->gracz->telefonDoPrzyjaciela);
		this->aktualnePytanie--;
		break;

	case 'P':
		this->gracz->uzyjKola(this->gracz->polNaPol);
		this->aktualnePytanie--;
		break;

	case 'K':
		this->gracz->uzyjKola(this->gracz->pytanieDoPublicznosci);
		this->aktualnePytanie--;
		break;

	case 'R':
		this->koniecGry(true);
		break;

	default:
		this->pytania.at(this->aktualnePytanie).wyswietlOdpowiedz(odp, 10);
		if (!this->pytania.at(this->aktualnePytanie).sprawdzOdpowiedz(odp) || this->aktualnePytanie == this->liczbaPytan - 1)
			this->koniecGry();
		else
			this->progi.wyswietlenieSum(this->aktualnePytanie);
		break;
	}
}

void Gra::koniecGry(bool rezygnacja)
{
	string wygrana;
	if (this->aktualnePytanie == this->liczbaPytan - 1)
		this->aktualnePytanie++; //zwiekszamy nr pytania zeby trafic w milion bo funkcja wygrana zmniejsza nr o 1
	wygrana = this->progi.wygrana(this->aktualnePytanie, rezygnacja);
	this->gracz->koniecGry(wygrana);
	system("cls");
	/*cout << "            _" << endl;
	cout << " || //" << "   //"<< endl;
	cout << " ||// " << "  || " << endl;
	cout << " ||\\\\ " << "  || " << endl;
	cout << " || \\\\" << "   \\\\ " << endl;*/
	cout << "Wygrana: " << wygrana << " zl" << endl;
	wstrzymaj();
	this->aktualnePytanie = this->liczbaPytan - 1;

	//zwalnianie pamieci
	delete this->gracz;
}

void Gra::wczytajPytania()
{
	this->pytania.clear();
	ifstream plik;
	plik.open(sciezka + "pytania.txt");
	if (plik.is_open())
	{
		while (!plik.eof())
		{
			string tresc, odp;
			bool popr;
			Pytanie pytanie;
			getline(plik, tresc);
			pytanie.setTresc(tresc);
			for (int i = 0; i < 4; i++)
			{
				popr = false;
				getline(plik, odp);
				if (odp[odp.length() - 1] == '1')
					popr = true;
				pytanie.wczytajOdpowiedz(odp.substr(0, odp.length() - 2), popr);
			}

			pytanie.przetasujOdpowiedzi();
			pytania.push_back(pytanie);
			random_shuffle(this->pytania.begin(), this->pytania.end(), myrand);
		}
		plik.close();
	}
	else
	{
		wstrzymaj("Brak pliku z pytaniami!!! Program zostanie zamkniety...");
		exit(0);
	}
}

void Gra::tabelaWynikow()
{
	fstream plik;

	plik.open(sciezka + "tabela.txt", ios::in);
	if (plik.good() == true)
	{

		while (!plik.eof())
		{
			string nick, kwota;
			time_t czas;
			plik >> nick;
			plik >> kwota;
			kwota = zastapZnaki(kwota, '_', ' ');
			plik >> czas;
			cout << nick << " " << kwota << " " << czas << endl;
		}
		plik.close();
	}
	else
		//wstrzymaj("Brak pliku z tabela wynikow!!!");
		cout << "Brak pliku z tabela wynikow!!!" << endl;
}

void wyswietlOpcje(string napis, bool selected = false, int ile = 1)
{
	Konsola::gotoXY(Konsola::getConsoleColumns() / 2 - (napis.length() / 2 + 2), Konsola::getCursorY());
	for (int i = 0; i < napis.length() + 4; i++)
		cout << "_";
	cout << endl;
	for (int i = 0; i < Konsola::getConsoleColumns() / 2 - (napis.length() / 2 + 3); i++)
		cout << "_";
	int pozycjaX = Konsola::getCursorX();
	int pozycjaY = Konsola::getCursorY();
	int pozycjaZaNapisem;
	for (int i = 0; i < ile; i++)
	{
		Konsola::gotoXY(pozycjaX, pozycjaY);
		if (selected && i % 2 == 0)
		{
			Konsola::setColor(secondaryColor);
		}
		cout << "/  " << napis << "  \\";
		pozycjaZaNapisem = Konsola::getCursorX();

		Konsola::gotoXY(Konsola::getConsoleColumns() / 2 - (napis.length() / 2 + 3), pozycjaY + 1);
		cout << "\\";
		for (int i = 0; i < napis.length() + 4; i++)
			cout << "_";
		cout << "/" << endl << endl;
		Konsola::setColor(primaryColor);

		if (selected)
			Sleep(200);
	}
	Konsola::gotoXY(pozycjaZaNapisem, pozycjaY);
	for (int i = 0; i < Konsola::getConsoleColumns() / 2 - (napis.length() / 2 + 3); i++)
	{
		if (napis.length() % 2 == 1 && i == 0)
			i++;
		cout << "_";
	}
	Konsola::gotoXY(0, pozycjaY + 3);
}


int znajdzIndeks(char literka, vector<string> opcje)
{
	int i = 0;
	while (i < opcje.size() && opcje.at(i)[0] != literka)
		i++;
	return i;
}

void Gra::inicjuj()
{
	setlocale(LC_ALL, "polish");
	while (1)
	{
		system("cls");
		vector<string> opcje;
		vector<int> pozycje;
		opcje.push_back("N - NOWA GRA");
		opcje.push_back("T - TABELA WYNIK�W");
		//opcje.push_back("U - USTAWIENIA");
		opcje.push_back("W - WYJ�CIE");
		//string opcje[3] = { "N - NOWA GRA", "T - TABELA WYNIKOW", "W - WYJSCIE" };
		//int pozycje[3];
		Konsola::setColor(primaryColor);
		cout << endl << endl;
		cout << "   		                           _                  _ _     _      _ _    _    _" << endl;
		cout << " 		||\\/||" << "  ||" << "  ||" << "     ||" << "   //   \\" << "\\" << "   ||\\   ||" << "  ||" << "      || \\" << "\\" << "   |_  / " << "  \\" << "\\  //" << endl;
		cout << " 		||\\/||" << "  ||" << "  ||" << "     ||" << "  ||     ||" << "  || \\  ||" << "  ||_ _" << "   || //" << "     // " << "    \\" << "\\//" << endl;
		cout << " 		||  ||" << "  ||" << "  ||   " << "  ||" << "  ||     ||" << "  ||  \\ ||" << "  ||" << "      ||\\" << "\\" << "     //_" << "      ||" << endl;
		cout << " 		||  ||" << "  ||" << "  ||_ _|" << " ||" << "   \\" << "\\ _ //" << "   ||   \\||" << "  ||_ _" << "   || \\" << "\\" << "   /_ _/" << "     ||" << endl << endl << endl;
		//PlaySound(TEXT("Milionerzy_intro.wav"), NULL, SND_SYSTEM);
		cout << endl;
		for each (string opcja in opcje)
		{
			pozycje.push_back(Konsola::getCursorY());
			wyswietlOpcje(opcja);
		}
		int ile = Konsola::getConsoleRows() - Konsola::getCursorY() - 2;
		for (int i = 0; i < ile; i++)
			cout << endl;
		//cout << endl << endl << endl << endl << endl << endl;


		char click;
		string click1;
		int indeks;
		cin >> click1;
		if (click1.length() > 1)
			click = 'X';
		else
			click = click1[0];
		Konsola::clearRow(Konsola::getCursorY() - 1);
		if (click >= 97)
			click -= 32;

		switch (click)
		{
		case 'N':
			indeks = znajdzIndeks(click, opcje);
			Konsola::gotoXY(0, pozycje.at(indeks));
			wyswietlOpcje(opcje.at(indeks), true, 5);
			system("cls");
			this->rozpocznij();
			break;
			
		case 'T':
			indeks = znajdzIndeks(click, opcje);
			Konsola::gotoXY(0, pozycje.at(indeks));
			wyswietlOpcje(opcje.at(indeks), true, 5);
			system("cls");
			this->tabelaWynikow();
			wstrzymaj(komunikatPause);
			system("cls");
			break;

		case 'W':
			indeks = znajdzIndeks(click, opcje);
			Konsola::gotoXY(0, pozycje.at(indeks));
			wyswietlOpcje(opcje.at(indeks), true, 5);
			exit(0);
			break;

		/*case 'U':
			primaryColor = FOREGROUND_GREEN;
			secondaryColor = BACKGROUND_GREEN;
			break;*/

		default:
			cout << "Zle wybrana opcja!!! ";
			wstrzymaj(komunikatPause);
			break;
		}

		/*if (click == 'n' || click == 'N')
		{
			Konsola::gotoXY(0, pozycje.at(0));
			wyswietlOpcje(opcje.at(0), true, 5);
			system("cls");
			this->rozpocznij();
		}
		else if (click == 't' || click == 'T')
		{
			Konsola::gotoXY(0, pozycje.at(1));
			wyswietlOpcje(opcje.at(1), true, 5);
			system("cls");
			this->tabelaWynikow();
			wstrzymaj(komunikatPause);
			system("cls");
		}
		else if (click == 'w' || click == 'W')
		{
			Konsola::gotoXY(0, pozycje.at(2));
			wyswietlOpcje(opcje.at(2), true, 5);
			exit(0);
		}
		else
		{
			cout << "Zle wybrana opcja!!! ";
			wstrzymaj(komunikatPause);
		}*/
	}
}