#pragma once
#include <fstream>
#include <Windows.h>
#include <MMSystem.h>
#include <vector>
#include "Pytanie.h"
#include "Gracz.h"
#include "Progi.h"
#include "Ustawienia.h"

using namespace std;

class Gra
{
private:
	vector<Pytanie> pytania; //kontener na dane, mozna zwiekszac. vector<typ> nazwa
	Gracz *gracz;
	Progi progi;
	int liczbaPytan;
	int aktualnePytanie;

public:
	Gra();
	~Gra();
	void inicjuj();
	void rozpocznij();
	Pytanie *generujPytanie();
	void wczytajPytania();
	void koniecGry(bool = false);
	void sprawdzOdpowiedz(char);
	void tabelaWynikow();
};

