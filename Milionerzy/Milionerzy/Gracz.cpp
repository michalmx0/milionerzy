#include "Gracz.h"

Gracz::Gracz(string nick, int wiek)
{
	this->nick = nick;
	this->wiek = wiek;
	time(&this->czas);
}

Gracz::~Gracz()
{
}

void Gracz::wyswietlPodpowiedzi()
{
	if (polNaPol.wykorzystane == false)
	{
		Konsola::gotoXY(72, 0);
		cout << "    _ _ _ _" << endl;
		Konsola::gotoXY(72, 1);
		cout << "  /         \\" << endl;
		Konsola::gotoXY(72, 2);
		cout << " |           |" << endl;
		Konsola::gotoXY(72, 3);
		cout << " |   50:50   |" << endl;
		Konsola::gotoXY(72, 4);
		cout << " |           |" << endl;
		Konsola::gotoXY(72, 5);
		cout << "  \\ _ _ _ _ /" << endl;
	}
	if (telefonDoPrzyjaciela.wykorzystane == false)
	{
		Konsola::gotoXY(86, 0);
		cout << "    _ _ _ _" << endl;
		Konsola::gotoXY(86, 1);
		cout << "  /         \\" << endl;
		Konsola::gotoXY(86, 2);
		cout << " |  TELEFON  |" << endl;
		Konsola::gotoXY(86, 3);
		cout << " |    DO     |" << endl;
		Konsola::gotoXY(86, 4);
		cout << " |PRZYJACIELA|" << endl;
		Konsola::gotoXY(86, 5);
		cout << "  \\ _ _ _ _ /" << endl;
	}
	if (pytanieDoPublicznosci.wykorzystane == false)
	{
		Konsola::gotoXY(100, 0);
		cout << "    _ _ _ _" << endl;
		Konsola::gotoXY(100, 1);
		cout << "  /       O \\" << endl;
		Konsola::gotoXY(100, 2);
		cout << " |    O  /|\\ |" << endl;
		Konsola::gotoXY(100, 3);
		cout << " |   /|\\  |  |" << endl;
		Konsola::gotoXY(100, 4);
		cout << " |    |  / \\ |" << endl;
		Konsola::gotoXY(100, 5);
		cout << "  \\ _/_\\_ _ /" << endl;
	}
	cout << endl << endl;
	cout << "------------------------------------------------------------------------------------------------------------------------" << endl;
	cout << "	  P - POL NA POL" << "           " << "    K - PYTANIE DO PUBLICZNOSCI" << "           " << "    T - TELEFON DO PRZYJACIELA" << endl;
	cout << endl << "                                          R - REZYGNACJA Z GRY " << endl;
	cout << "------------------------------------------------------------------------------------------------------------------------" << endl;
	cout << endl << endl;
}

void Gracz::pytanie(Pytanie &pytanie)
{
	this->aktualnePytanie = &pytanie;
	system("cls");
	this->wyswietlPodpowiedzi();
	this->aktualnePytanie->wyswietl();
}

void Gracz::koniecGry(string wygrana)
{
	time_t czas;
	time(&czas);
	this->czas = czas - this->czas;
	ifstream temp;
	ofstream plik;
	temp.open(sciezka + "tabela.txt");
	plik.open(sciezka + "tabela.txt", ios::app);
	if (temp.good())
	{
		temp.close();
		plik << endl;
	}
	wygrana = zastapZnaki(wygrana, ' ', '_');
	plik << this->nick << " " << wygrana << " " << this->czas;
	plik.close();

}

char Gracz::odpowiedz()
{
	char opcja;
	string odpowiedz;
	cout << endl;
	int posY = Konsola::getCursorY();
	cin >> odpowiedz;
	if (odpowiedz.length() > 1)
		opcja = 'X';
	else
		opcja = odpowiedz[0];
	Konsola::clearRow(posY);
	if (opcja >= 97)
		opcja -= 32;
	if (opcja == 'A' || opcja == 'B' || opcja == 'C' || opcja == 'D' || opcja == 'T' || opcja == 'P' || opcja == 'K' || opcja == 'R')
		return opcja;
	else
	{
		cout << "Wybrano bledna opcje! ";
		wstrzymaj(komunikatPause);
		this->pytanie(*this->aktualnePytanie);
		return this->odpowiedz();
	}
}