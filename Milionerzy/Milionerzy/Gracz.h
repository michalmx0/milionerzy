#pragma once
#include <fstream>
#include "Pytanie.h"
#include "PolNaPol.h"
#include "PytanieDoPublicznosci.h"
#include "TelefonDoPrzyjaciela.h"
#include "Ustawienia.h"

using namespace std;

class Gracz
{
	friend class Gra;
private:
	string nick;
	int wiek;
	Pytanie *aktualnePytanie;
	time_t czas;

protected:
	PolNaPol polNaPol;
	PytanieDoPublicznosci pytanieDoPublicznosci;
	TelefonDoPrzyjaciela telefonDoPrzyjaciela;

public:
	Gracz(string, int);
	~Gracz();
	char odpowiedz();
	void wyswietlPodpowiedzi();
	void pytanie(Pytanie&);
	template <class T>
	void uzyjKola(T&);
	void koniecGry(string);
};



template <class T>
void Gracz::uzyjKola(T &kolo)
{
	if (!kolo.wykorzystane)
	{
		kolo.wykorzystaj(*aktualnePytanie);
		system("cls");
		this->wyswietlPodpowiedzi();
		kolo.wykorzystane = true;
	}
	else
	{
		cout << "Juz wykorzystales to kolo!" << endl;
		system("pause");
	}
}