#pragma once
#include <iostream>
#include <string>
#include "Pytanie.h"

using namespace std;

class KoloRatunkowe
{
	friend class Pytanie;
	friend class Gracz;
private:
	string nazwa;

protected:
	bool wykorzystane;

public:
	KoloRatunkowe();
	~KoloRatunkowe();
	virtual void wykorzystaj(Pytanie&) = 0;
};

