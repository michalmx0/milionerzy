#include "Konsola.h"

void Konsola::gotoXY(int x, int y)
{
	COORD p = { x, y };
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), p);
}

int Konsola::getConsoleColumns()
{
	CONSOLE_SCREEN_BUFFER_INFO csbi;

	GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi);
	return csbi.srWindow.Right - csbi.srWindow.Left + 1;
}

int Konsola::getConsoleRows()
{
	CONSOLE_SCREEN_BUFFER_INFO csbi;

	GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi);
	return csbi.srWindow.Bottom - csbi.srWindow.Top + 1;
}

int Konsola::getCursorX()
{
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	COORD                      result;
	if (!GetConsoleScreenBufferInfo(
		GetStdHandle(STD_OUTPUT_HANDLE),
		&csbi
	))
		return -1;
	return csbi.dwCursorPosition.X;
}

int Konsola::getCursorY()
{
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	COORD                      result;
	if (!GetConsoleScreenBufferInfo(
		GetStdHandle(STD_OUTPUT_HANDLE),
		&csbi
	))
		return -1;
	return csbi.dwCursorPosition.Y;
}

void Konsola::setColor(int color)
{
	HANDLE hOut;
	hOut = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hOut, color | FOREGROUND_INTENSITY);
}

void Konsola::clearRow(int posY)
{
	Konsola::gotoXY(0, posY);
	for (int i = 0; i < Konsola::getConsoleColumns(); i++)
		cout << " ";
	Konsola::gotoXY(0, posY);
}

void Konsola::center(int posY, int max)
{
	Konsola::gotoXY((Konsola::getConsoleColumns() - max) / 2, posY);
}

void Konsola::setCoding(int coding)
{
	SetConsoleOutputCP(coding);
}
