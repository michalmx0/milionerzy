#pragma once
#include <Windows.h>
#include <iostream>
#include <string>

using namespace std;

class Konsola
{
public:
	static void gotoXY(int, int);
	static int getConsoleColumns();
	static int getConsoleRows();
	static int getCursorX();
	static int getCursorY();
	static void setColor(int);
	static void clearRow(int);
	static void center(int, int);
	static void setCoding(int);
};

