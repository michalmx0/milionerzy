#pragma once
#include <string>

using namespace std;

class Odpowiedz
{
	friend class Pytanie;
protected:
	string tresc;
	bool poprawna;
public:
	Odpowiedz(string, bool); //konstruktor wieloargumentowy
	~Odpowiedz();
};

