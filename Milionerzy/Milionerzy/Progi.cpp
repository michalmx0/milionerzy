#include "Progi.h"



Progi::Progi()
{
}


Progi::~Progi()
{
}

int Progi::wczytajZPliku()
{
	int liczbaPytan = 0;
	ifstream plik;
	plik.open(sciezka + "progi.txt");
	if (plik.is_open())
	{
		while (!plik.eof())
		{
			string prog;
			getline(plik, prog);
			if (prog[prog.length() - 1] == 'g')
			{
				this->gwarantowane.push_back(1);
				this->kwoty.push_back(prog.substr(0, prog.length() - 2));
			}
			else
			{
				this->gwarantowane.push_back(0);
				this->kwoty.push_back(prog);
			}
			liczbaPytan++;
		}
		plik.close();
		if (liczbaPytan == 0)
		{
			wstrzymaj("Brak progow w pliku z programi!!! Program zostanie zamkniety...");
			exit(0);
		}
	}
	else
	{
		wstrzymaj("Brak pliku z progami!!! Program zostanie zamkniety...");
		exit(0);
	}
	return liczbaPytan;
}

void Progi::wyswietlenieSum(int nrPytania)
{
	int max = 42;
	system("cls");
	cout << endl << endl << endl << endl << endl;
	for (int i = this->kwoty.size() - 1; i > 0; i--)
	{
		Konsola::center(Konsola::getCursorY(), max + 3);
		if (nrPytania >= i)
			Konsola::setColor(secondaryColor);
		if (i < 10)
			cout << " ";
		cout << "  " << i;
		for (int j = 0; j < max - this->kwoty.at(i).length() - 3; j++)
			cout << " ";
		cout << this->kwoty.at(i) << " zl" << endl;
	}
	Konsola::setColor(primaryColor);
	cout << endl << endl << endl;
	Konsola::center(Konsola::getCursorY(), komunikatPause.length());
	wstrzymaj(komunikatPause);
}

string Progi::wygrana(int nrPytania, bool rezygnacja)
{
	if (rezygnacja)
		return this->kwoty.at(nrPytania - 1);
	else
		return znajdzProg(nrPytania - 1);
}

string Progi::znajdzProg(int nrPytania)
{
	while (nrPytania != -1)
	{
		if (this->gwarantowane.at(nrPytania))
			return this->kwoty.at(nrPytania);
		nrPytania--;
	}
}
