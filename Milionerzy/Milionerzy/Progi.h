#pragma once
#include <fstream>
#include <vector>
#include "Ustawienia.h"

using namespace std;

class Progi
{
private:
	vector<string> kwoty;
	vector<bool> gwarantowane;

public:
	Progi();
	~Progi();
	int wczytajZPliku();
	void wyswietlenieSum(int);
	string wygrana(int, bool = false);
	string znajdzProg(int);
};

