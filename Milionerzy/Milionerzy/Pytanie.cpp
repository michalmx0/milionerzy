#include "Pytanie.h"
#include <iterator>

Pytanie::Pytanie()
{
	this->literki.push_back('A');
	this->literki.push_back('B');
	this->literki.push_back('C');
	this->literki.push_back('D');
}

Pytanie::~Pytanie()
{
}

bool Pytanie::sprawdzOdpowiedz(char odp)
{
	int i;
	for (i = 0; i < 4; i++)
	{
		if (this->literki.at(i) == odp)
			break;
	}
	return odpowiedzi.at(i).poprawna;
}

void Pytanie::wyswietl()
{
	cout << " ";
	for (int i = 0; i < this->tresc.length()+4; i++)
	{
		cout << "_";
	}
	cout << endl;
	cout << "/  "<< this->tresc<<"  \\"<<endl;
	cout << "\\";
	for (int i = 0; i < this->tresc.length() + 4; i++)
	{
		cout << "_";
	}
	cout << "/"<<endl;
	//odp
	/*int max = 0;
	for (int i = 0; i < 4; i++)
	{
		if (this->odpowiedzi.at(i).tresc.length()>max)
			max = this->odpowiedzi.at(i).tresc.length();
	}
	max += 6;
	cout << " ";
	for (int i = 0; i < max; i++)
	{
		cout << "_";
	}

	for (int i = 0; i < 4; i++)
	{
		cout << " ";
		cout << endl;
		this->posY[i] = Konsola::getCursorY();
		cout << "/  " << this->literki[i] << ":" << this->odpowiedzi.at(i).tresc;
		int c = max - this->odpowiedzi.at(i).tresc.length() - 4;
		for (int j = 0; j < c; j++)
		{
			cout << " ";
		}
		cout << "\\" << endl;
		cout << "\\";
		for (int j = 0; j < max; j++)
		{
			cout << "_";
		}

		cout << "/";
	}*/
	this->posY[0] = Konsola::getCursorY() + 1;
	for (int i = 0; i < 4; i++)
		this->wyswietlOdpowiedz(this->literki.at(i));
	//cout << endl;
}

void Pytanie::wyswietlOdpowiedz(char odp, int ile)
{
	// wyznacz ile potrzeba miejsca na tekst odpowiedzi
	int max = 0;
	for (int i = 0; i < 4; i++)
	{
		if (this->odpowiedzi.at(i).tresc.length() > max)
			max = this->odpowiedzi.at(i).tresc.length();
	}
	max += 7;

	int i = 0;
	while (this->literki[i] != odp)
		i++;

	//wypisz gorne obramowanie pierwszej odpowiedzi 
	if (i == 0)
	{
		Konsola::gotoXY(0, this->posY[0] - 1);
		cout << " ";
		for (int i = 0; i < max; i++)
		{
			cout << "_";
		}
		cout << endl;
	}

	//wypisuj odpowiedzi
	for (int k = 0; k < ile; k++)
	{
		Konsola::gotoXY(0, this->posY[i]);
		if (k == 0 && ile != 1)
		{
			Konsola::setColor(secondaryColor);
		}
		if (k % 2 == 1)
		{
			if (this->odpowiedzi.at(i).poprawna)
				Konsola::setColor(BACKGROUND_GREEN);
			else
				Konsola::setColor(BACKGROUND_RED);
		}

		cout << "/  ";
		if (this->odpowiedzi.at(i).tresc.length() > 0)
			cout << this->literki[i] << ": ";
		else
			cout << "   ";
		cout << this->odpowiedzi.at(i).tresc;
		int c = max - this->odpowiedzi.at(i).tresc.length() - 5;
		for (int j = 0; j < c; j++)
		{
			cout << " ";
		}
		cout << "\\" << endl;
		cout << "\\";
		for (int j = 0; j < max; j++)
		{
			cout << "_";
		}

		cout << "/";

		//jesli nie jest to pierwsze wyswietlenie pytan
		if (ile != 1)
		{
			if (k == 0) // zaznaczenie
				Sleep(1500);
			else
				Sleep(200); //miganie
		}
		Konsola::setColor(primaryColor);
	}
	cout << endl;
	//pobierz pozycje kolejnej odpowiedzi
	if (ile == 1)
		this->posY[i+1] = Konsola::getCursorY();
	//jesli sprawdzano zaznaczona odpowiedz to skocz na sam dol
	if (odp != '_')
		Konsola::gotoXY(0, this->posY[4]);
}

void Pytanie::wczytajOdpowiedz(string tresc, bool poprawnosc)
{
	Odpowiedz nowa(tresc, poprawnosc);
	this->odpowiedzi.push_back(nowa);
}

string Pytanie::getOdpowiedzi()
{
	string odpowiedzi = "";
	int i = 0;
	for ( Odpowiedz odpowiedz : this->odpowiedzi)
	{
		if (odpowiedz.tresc != "")
			odpowiedzi += to_string(++i);
	}
	return odpowiedzi;
}

void Pytanie::przetasujOdpowiedzi()
{
	random_shuffle(this->odpowiedzi.begin(), this->odpowiedzi.end(), myrand);
}

void Pytanie::polNaPol()
{
	srand(time(NULL));
	int losuj;
	losuj = rand() % 4;
	while (this->odpowiedzi.at(losuj).poprawna)
	{
		losuj = rand() % 4;
	}
	for (int i = 0; i < 4; i++)
	{
		if (this->odpowiedzi.at(i).poprawna == 0 && i != losuj)
			this->odpowiedzi.at(i).tresc = "";
	}
}
