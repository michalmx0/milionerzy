#pragma once
#include <vector>
#include <algorithm>
#include "Odpowiedz.h"
#include "Ustawienia.h"

using namespace std;

class Pytanie
{
private:
	string tresc;
	vector<Odpowiedz> odpowiedzi;
	vector<char> literki;
	int posY[5];

public:
	Pytanie();
	~Pytanie();
	bool sprawdzOdpowiedz(char);
	void wczytajOdpowiedz(string, bool);
	void polNaPol();
	void wyswietl();
	void wyswietlOdpowiedz(char = '_', int = 1);
	void setTresc(string tresc) { this->tresc = tresc; }
	string getOdpowiedzi();
	void przetasujOdpowiedzi();
};

