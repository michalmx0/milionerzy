#include "PytanieDoPublicznosci.h"


PytanieDoPublicznosci::PytanieDoPublicznosci()
{
}


PytanieDoPublicznosci::~PytanieDoPublicznosci()
{
}

void PytanieDoPublicznosci::wykorzystaj(Pytanie &pytanie)
{
	srand(time(NULL));
	pytanie.getOdpowiedzi();
	int losuj;
	losuj = 1 + rand() % 4;
	if (losuj == 1)
	{
		cout << endl << endl;
		cout << "60%" << "	" << "20%" <<"	" << "5%" << "	" << "15%" << "	" << endl;
		cout << " _ "  << "  " << "   " <<"	" << "  " << "  " << "   "<<endl;
		cout << "| |" << "	" << "   "  <<"	" << "  " << "	" << "   " << endl;
		cout << "| |" << "	" << " _ "  <<"	" << "  " << "	" << "   " << endl;
		cout << "| |" << "	" << "| |" <<"	" << "  " << "	"<<  " _ "<<endl;
		cout << "| |" << "	" << "| |" <<"	" << " _"  << "	"<<  "| |"<<endl;
		cout << "| |" << "	" << "| |" <<"	" <<"| |" << "	"<<  "| |"<< endl<<endl;
		cout << " A " << "   " << "   B " <<"	" << " C "  << "	" << " D " << endl;
		
	}
	if (losuj == 2)
	{
		cout << endl;
		cout << "30%" << "	" << "50%" << "	" << " 2%" << "	" << " 18%"  << endl;
		cout << "  " << "	" << "  " << endl;
		cout << "  " << "	" << " _" << "	" << " " << "	" <<  "   " << endl;
		cout << " _" << "	" << "| |" << "	" << " " << "	" <<  "   " << endl;
		cout << "| |" << "	" << "| |" << "	" << " " << "	" <<  " _ " << endl;
		cout << "| |" << "	" << "| |" << "	" << " _" <<"	" <<  "| |" << endl;
		cout << "| |" << "	" << "| |" << "	" << "| |" <<"   "<< "  | |" << endl<<endl;
		cout << " A " << "   " << "   B " << "	" << " C " << "	" << " D " << endl;
	}
	if (losuj == 3)
	{
		cout << endl;
		cout << " 90%" << "	" << " 1%" <<"	" << " 8%" << "	" << " 1%" << "	" << endl;
		cout << "  _" << "	" << "  " << endl;
		cout << " | |" << "	" << "  " << "	" << " " << "	" << "  " << endl;
		cout << " | |" << "	" << "  " << "	" << " " << "	" << "  " << endl;
		cout << " | |" << "	" << "  " << "	" << " " << "	" << "  " << endl;
		cout << " | |" << "	" << "  " << "	" << " _" <<"	" << "  " << endl;
		cout << " | |" << "	" << " _" << "	" << "| |"<<"	" << " _" << endl;
		cout << " | |" << "	" << "| |" <<"	" << "| |"<<"	" << "| |" << endl<<endl;
		cout << "  A " << "   " << "  B " << "	" << " C " << "	" << " D " << endl;
	}
	if (losuj == 4)
	{
		cout << endl;
		cout << " 8%" << "	" << " 7%" << "	" << " 60%" << "	" << " 25%" << "	" << endl;
		cout << "  " << "	" << "  " << endl;
		cout << "  " << "	" << "  " << "	" << " _ " << "	 " << "  " << endl;
		cout << "  " << "	" << " " << "	" << "| |" << "	 " << " _" << endl;
		cout << " _ " << "	" << " _" << "	" << "| |" << "	 " << "| |" << endl;
		cout << "| |" << "	" << "| |" << "	" << "| |" << "	 " << "| |" << endl;
		cout << "| |" << "	" << "| |" << "	" << "| |" << "  " << "    | |" << endl<<endl;
		cout << " A " << "   " << "   B " << "	" << " C " << "	" << "  D " << endl;
	}
	system("pause");
}