#include "Ustawienia.h"

string sciezka = "pliki/";
string komunikatPause = "Naci�nij dowolny przycisk aby kontynuowa�...";
int primaryColor = FOREGROUND_BLUE;
int secondaryColor = BACKGROUND_BLUE;

string zastapZnaki(string text, char a, char b) {
	for (string::iterator it = text.begin(); it != text.end(); ++it) {
		if (*it == a) {
			*it = b;
		}
	}
	return text;
}

int myrand(int i)
{
	srand(time(NULL));
	return  rand() % i;
}

void wstrzymaj(string komunikat)
{
	cout << komunikat;
	cin.ignore(INT_MAX, '\n');
	cin.clear();
	//if (komunikat != "")
	cin.get();
}