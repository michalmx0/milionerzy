#pragma once
#include "Konsola.h"
#include <iterator>
#include <ctime>

using namespace std;

extern string sciezka;
extern string komunikatPause;
extern int primaryColor;
extern int secondaryColor;

extern string zastapZnaki(string, char, char);
extern int myrand(int);
extern void wstrzymaj(string = "");
